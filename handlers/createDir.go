package handlers

import (
	"fmt"
	"os"
	"path/filepath"
)

func CreateDir(username string){
	wd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}

	path := filepath.Join(wd, "gallery", username)
	if _, err := os.Stat(path)
	os.IsNotExist(err) {
		err = os.Mkdir(path, 0755)
		if err != nil {
			fmt.Println(err)
		}
	}
}
