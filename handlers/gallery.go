package handlers

import (
	"crypto/sha1"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func Feed(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		mf, fh, err := req.FormFile("nf")
		if err != nil {
			fmt.Println(err)
		}
		defer mf.Close()
		ext := strings.Split(fh.Filename, ".")[1]
		h := sha1.New()
		io.Copy(h, mf)
		fname := fmt.Sprintf("%x", h.Sum(nil)) + "." + ext
		// create new file
		wd, err := os.Getwd()
		if err != nil {
			fmt.Println(err)
		}

		path := filepath.Join(wd, "gallery", Name, fname)
		// if path err, create dir
		nf, err := os.Create(path)
		if _, err := os.Stat(path)
		os.IsNotExist(err){
			CreateDir(Name)
		}

		if err != nil {
			fmt.Println(err)
		}
		defer nf.Close()
		mf.Seek(0, 0)
		io.Copy(nf, mf)
	}

	file, err := os.Open("gallery/")
	if err != nil {
		log.Fatalf("failed opening directory: %s", err)
	}
	defer file.Close()

	dirs := []string{}

	err = filepath.Walk("gallery",
	    func(path string, info os.FileInfo, err error) error {
		        if err != nil {
				return err
			}
			dirs = append(dirs, path)
			return nil
		})
		if err != nil {
		        log.Println(err)
		}

	tpl.ExecuteTemplate(w, "index.gohtml", dirs)
}
