package main

import (
	"net/http"

	"gitlab.com/kamee/picverse/handlers"
)

func main() {
	handlers.ConnectDatabase()

	http.HandleFunc("/feed", handlers.Feed)
	http.Handle("/gallery/", http.StripPrefix("/gallery", http.FileServer(http.Dir("./gallery"))))
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.HandleFunc("/", handlers.RenderLogin)
	http.HandleFunc("/register", handlers.RenderRegister)
	http.HandleFunc("/signin", handlers.SignInUser)
	http.HandleFunc("/signup", handlers.SignUpUser)
	http.HandleFunc("/profile", handlers.RenderHome)
	http.HandleFunc("/userDetails", handlers.GetUserDetails)

	http.ListenAndServe(":8090", nil)
}
